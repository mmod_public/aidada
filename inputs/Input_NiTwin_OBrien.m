%AIDADA input file for Ni Twin case study from:
%Gordon and Sills, J. Mech. Phys. Sol., 2024
A  = "FCC" ;
B  = "FCC" ; 
aA = 0.352 ;
aB = 0.352 ;
unt = 'nm';
%natural state orientations
yAn = [ 17 ; 17 ; -20 ];
yBn = [ -17 ; -17 ; -20 ];
xAn = [ 1 ; -1 ; 0 ];
xBn = [ 1 ; -1 ; 0 ];
%coherent reference state orientations
yAc = [ 1 ; 1 ; -1 ];
yBc = [ -1 ; -1 ; -1 ];
xAc = xAn;
xBc = xBn;
srt = 3;
dmax = 1e4;
BurgersList = {'BurgersFCC.txt','BurgersFCC.txt','BurgersOBrien.txt'};
CrysList = ["A","B","INT"];
doselfcon = 0;   %flag to indicate whether to enforce self consistency
norepeats = 1;

fname = 'results/NiTwin_noselfcon.txt';
 
AIDADA(A,B,aA,aB,xAn,xBn,yAn,yBn,xAc,xBc,yAc,yBc,unt,srt,...
       BurgersList,CrysList,eye(3),eye(3),eye(3),dmax,doselfcon,norepeats,fname)
