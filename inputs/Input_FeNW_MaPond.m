%AIDADA input file for Fe Nishiyama-Wasserman case study from:
%Gordon and Sills, J. Mech. Phys. Sol., 2024
A  = "FCC";
B  = "BCC"; 
aA = 0.3580 ;
aB = 0.2870 ;
unt = 'nm';
%natural state orientations
yAn = [ 1 ; 1 ; 1 ];
yBn = [ 0 ; 1 ; -1 ];
xAn = [ 1 ; -1 ; 0 ];
xBn = [ 1 ; 0 ; 0 ];
%coherent reference state is same (empty vectors inputted below)
srt = 3;
dmax = 1e4;
BurgersList = {'BurgersFCC.txt','BurgersMaPond.txt','BurgersBCC.txt'};
CrysList = ["A","INT","B"];
SA = [ 1.0669     0         0
         0         1     0
         0         0     0.9628];
SB =    [0.9410         0         0
         0         1         0
         0         0    1.0401];
doselfcon = 1;   %flag to indicate whether to enforce self consistency
norepeats = 1;

fname = 'results/Fe_NW_selfcon.txt'; 
 
AIDADA(A,B,aA,aB,xAn,xBn,yAn,yBn,[],[],[],[],unt,srt,...
       BurgersList,CrysList,SA,SB,eye(3),dmax,doselfcon,norepeats,fname)
