A  = "FCC";
B  = "BCC"; 
aA = 0.3580 ;
aB = 0.2870 ;
unt = 'nm';
%natural state orientations
yAn = [ 1 ; 1 ; 1 ];
yBn = [ 0 ; 1 ; -1 ];
xAn = [ 1 ; -1 ; 0 ];
xBn = [ 1 ; 0 ; 0 ];
%10 deg tilt 
yAn = rotm(1,10)*yAn;
xAn = rotm(1,10)*xAn;
%coherent reference state is same (empty vectors inputted below)
yAc = [ 1 ; 1 ; 1 ];
yBc = [ 0 ; 1 ; -1 ];
xAc = [ 1 ; -1 ; 0 ];
xBc = [ 1 ; 0 ; 0 ];
srt=3 ;
dmax = 1e4;
BurgersList = {'BurgersFCC.txt','BurgersMaPond.txt','BurgersBCC.txt'};
CrysList = ["A","INT","B"];
SA = [ 1.0669     0         0
         0         1     0
         0         0     0.9628];
SB = [0.9410         0       0
         0         1         0
         0         0    1.0401];
doselfcon = 1;   %flag to indicate whether to enforce self consistency
norepeats = 1;
fname = 'results/Fe_NW_10dx_selfcon.txt'; 
AIDADA(A,B,aA,aB,xAn,xBn,yAn,yBn,xAc,xBc,yAc,yBc,unt,srt,...
       BurgersList,CrysList,SA,SB,eye(3),dmax,doselfcon,norepeats,fname)



function R = rotm(axis,ang)
% inputs: x=1,y=2,z=3
if axis == 1 || 'x'
    R = [1 0 0; 0 cosd(ang) -sind(ang); 0 sind(ang) cosd(ang)];
elseif axis == 2 || 'y'
    R = [cosd(ang) 0 sind(ang); 0 1 0; -sind(ang) 0 cosd(ang)];
elseif axis == 3 || 'z'
    R = [cosd(ang) -sind(ang) 0; sind(ang) cosd(ang) 0; 0 0 1];
end
end