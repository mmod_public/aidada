%rotate vector u about axis ax by rotation angle phi
%phi must be in radians
function v = axis_angle_rotate(u,ax,phi)
ax = ax/norm(ax);
xrot = ax(1);
yrot = ax(2);
zrot = ax(3);
Rot = [cos(phi)+xrot^2*(1-cos(phi)), xrot*yrot*(1-cos(phi))-zrot*sin(phi), xrot*zrot*(1-cos(phi))+yrot*sin(phi);
        yrot*xrot*(1-cos(phi))+zrot*sin(phi), cos(phi)+yrot^2*(1-cos(phi)), yrot*zrot*(1-cos(phi))-xrot*sin(phi);
        zrot*xrot*(1-cos(phi))-yrot*sin(phi), zrot*yrot*(1-cos(phi))+xrot*sin(phi), cos(phi)+zrot^2*(1-cos(phi))];
v = Rot*u;