function AIDADA(Astruct,Bstruct,aA,aB,xAn,xBn,yAn,yBn,xAc,xBc,yAc,yBc,unt,srt,...
                         BurgersList,CrysList,SA,SB,Fr,dmax,doselfcon,norepeats,fname)
      
% ------------------------------------------------------
% Enumerate all solutions to the Frank-Bilby equation for the given crytal
% orientations, coherency strains, and set of line defects (dislocations
% and disconnections)
%
% For details of the method see:
% D. B. Gordon and R. B. Sills, J. Mech. Phys. Sol., In-Press, 105845, 2024.
%
% Terrace plane normal vector is along the y-axis 
% ------------------------------------------------------

%
%                       Crystal A
%                                   _ _ _ _ _
%                          _ _ _ _ |
%     ^            _ _ _ _|
%    y|    _ _ _ _|
%     |  
%                       Crystal B
%

% --- INPUT
% Astruct       Crystal structure type for crystal A
% Bstruct       Crystal structure type for crystal B
% aA            Lattice constant of Crystal A
% aB            Lattice constant of Crystal B

% xAn           x direction of Crystal A in the natural state
% yAn           y direction of Crystal A in the natural state
% xBn           x direction of Crystal B in the natural state
% yBn           y direction of Crystal B in the natural state

% if any of the following four vectors are empty [], the corresponding
% natural state vector is used in its place
% xAc           x direction of Crystal A in the coherent reference state
% yAc           y direction of Crystal A in the coherent reference state
% xBc           x direction of Crystal B in the coherent reference state
% yBc           y direction of Crystal B in the coherent reference state

% unt           unit used for the lattice parameter (string)
% srt	        Solution sorting parameter (P=1 | Q=2 | R=3) (integer)

% Burgers_List  Cell array of strings referring to text files containing
%               Burgers vectors to use in the calculation
% CrysList      String array specifying the coordinate system used to 
%               define each respective Burgers_List entry; A = Crystal A, 
%               B = Crystal, INT = interface

% SA            Stretch tensor for the natural state of Crystal A
% SB            Stretch tensor for the natural state of Crystal B
% Fr            Deformation gradient to deviate from the default coherent
%               reference state, if desired

% dmax          maximum defect spacing allowed, if d>dmax it is set to inf
% doselfcon     Binary flag (0 or 1) indicating whether to enforce
%               self-consistency in the habit plane orientation
% norepeats     Binary flag (0 or 1) indicating whether to remove repeated
%               solutions from the solution list
% fname         String indicating the file name (plus extension) to write 
%               results to
 
% Original version (AIDA) released December 2017 by:
% Ali Sangghaleh (alisang@tamu.edu)
% Michael J. Demkowicz (demkowicz@tamu.edu)
% Method described in:
% A. Sangghaleh and M. J. Demkowicz, Comp. Mater. Sci., Vol. 145, p. 35-47,
% 2018.

% This version (AIDADA) released September 2024 by
% Ryan Sills (ryan.sills@rutgers.edu)
% David Gordon (dbg68@scarletmail.rutgers.edu)

eps1 =1E-9;  

if doselfcon
    fprintf('enforcing habit plane self-consistency...\n')
end

% Rotation matrices from unit cell frame to natural frame
xAn=transpose(xAn)/norm(xAn);
xBn=transpose(xBn)/norm(xBn);
yAn=transpose(yAn)/norm(yAn);
yBn=transpose(yBn)/norm(yBn);
RAn = [ xAn ; yAn ; cross(xAn,yAn) ];
RBn = [ xBn ; yBn ; cross(xBn,yBn) ];
%we assume that the natural state is misfit free
FAn = RAn;
FBn = RBn;

% Rotation matrices from unit cell frame to coherent reference frame
% if no vectors are given, the natural vectors are used
if isempty(xAc)
    xAc = xAn;
else
    xAc=transpose(xAc)/norm(xAc);
end
if isempty(xBc)
    xBc = xBn;
else
    xBc=transpose(xBc)/norm(xBc);
end
if isempty(yAc)
    yAc = yAn;
else
    yAc=transpose(yAc)/norm(yAc);
end
if isempty(yBc)
    yBc = yBn;
else
    yBc=transpose(yBc)/norm(yBc);
end
RAc = [ xAc ; yAc ; cross(xAc,yAc) ];
RBc = [ xBc ; yBc ; cross(xBc,yBc) ];

% ----------------------------- Check Handedness
if norm(RAn*RAn'-eye(3))>eps1
    error('Coordinate axes for crystal A are invalid');
elseif norm(RBn*RBn'-eye(3))>eps1
    error('Coordinate axes for crystal B are invalid');
elseif norm(RAc*RAc'-eye(3))>eps1
    error('Reference coordinate axes for crystal A are invalid');
elseif norm(RBc*RBc'-eye(3))>eps1
    error('Reference coordinate axes for crystal B are invalid');
end

% Map from unit cell frame to the coherent reference frame, including
% stretches and change from the default given by Fr
FAc = Fr*SA*RAc;
FBc = Fr*SB*RBc;

if (srt~=0 && srt~=1 && srt~=2 && srt~=3)
    error('Error 2! srt must be 0, 1, 2, or 3');
end

% Loop over the list of Burgers vector files, load them all, adjust into
% the reference frame as appropriate
blist = [];
hlist = [];
for i=1:length(BurgersList)
    %makes sure the line defects are being associated with the proper
    %crystal structure
    fid = fopen(['burgers/' BurgersList{i}]);
    txt = textscan(fid,'%s','delimiter','\n');
    crysi = txt{1}{2};
    if (CrysList{i}=="A" && crysi~=Astruct) || (CrysList{i}=="B" && crysi~=Bstruct)
            error('%s for crystal type %i assigned to incombatible crystal %s',BurgersList{i},crysi,CrysList{i})     
    end
    
    %grab the Burgers vector
    dati = dlmread(['burgers/' BurgersList{i}],',',3,0);
    bi = dati(:,1:3);
    
    if crysi=="INT"
        %defects are already in the coherent reference state, except possible
        %change due to Fr, scaled based on average lattice constant
        aAve = (aA+aB)/2;
        blist=[blist;(aAve*Fr*bi')'];
        hlist = [hlist; dati(:,4)*aAve];
    else
        %defects are in a unit cell coordinate system
        %Transform into the coherent reference state, scale based on lattice
        %constant
        if (CrysList{i}=="A")
            blist=[blist;(aA*FAc*bi')'];
            hlist = [hlist; dati(:,4)*aA];
        elseif (CrysList{i}=="B")
            blist=[blist;(aB*FBc*bi')'];
            hlist = [hlist; dati(:,4)*aB];
        else
            error('Invalid crystal ID for %s, must be A, B,or INT',BurgersList{i})
        end
    end
end

%construct a comprehensive list of linearly independent Burgers vector 
%triplets
[bG,hG] = LITriplet(blist,hlist);
nb = size(bG,1);
if nb==0
    error('No linearly independent Burgers vectors triples with one or fewer disconnections exist!')
end

%Grab all of the symmetry operators
[MMA]=SymElm(Astruct);          
ncoA = length(MMA);
[MMB]=SymElm(Bstruct);          
ncoB = length(MMB);

%Loop over all combinations of symmetry operators and sets of Burgers
%vectors
Mat = zeros(2*ncoA*ncoB*nb,51); %initialize twice as much in case every solution has two roots (conservative)
no=0;
index=0;
fprintf('Total number of possible solutions: %d\n',ncoA*ncoB*nb)
for i=1:ncoA  %symmetry operators for crystal A
    UA = MMA{i};
    %map from coherent reference frame to the natural reference frame
    invFA = inv(FAn*inv(FAc*UA));
    for j=1:ncoB %symmetry operators for crystal B
        UB = MMB{j};
        invFB = inv(FBn*inv(FBc*UB));
        
        T=invFA-invFB;

        for k=1:nb
            b1=bG(k,1:3)';
            b2=bG(k,4:6)';
            b3=bG(k,7:9)';
            h=hG(k,1);
            
            % bstar vectors
            c12=cross(b1,b2);
            c23=cross(b2,b3);
            c31=cross(b3,b1);
            bs1=c23/(dot(b1,c23));
            bs2=c31/(dot(b2,c31));
            bs3=c12/(dot(b3,c12));


            % O-lattice planes: Orientation
            T1=transpose(T)*bs1;
            T2=transpose(T)*bs2;
            T3=transpose(T)*bs3;

            %if a disconnection may be present in the array, find the
            %appropriate habit plane
            if norm(T1)>eps1
                if doselfcon
                    [n,flag,angs] = find_habit_plane(h,[0;1;0],T1);
                else
                    n = [0; 1; 0];
		            angs = [0 0];
                    flag = 0;
                end
                %if the solution was singular (d1->inf), set n1 to 0 so
                %it is skipped below
                n1 = []; nT1 = [];
                %loop in case multiple solutions were found
                for l=1:size(n,2)
                    if l==2
                        fprintf('Multiple solutions found for case no. %i\n',index+1); 
                    end
                    if norm(n(:,l))==0
                        n(:,l) = [0; 1; 0];
                        n1(l) = 0;
                    else
                        nT1(:,l) = cross(T1,n(:,l));
                        n1(l) = norm(nT1(:,l));
                    end
                end
            else
                n = [0; 1; 0];
                nT1 = [0; 0; 0];
                n1 = 0;
            end

            %if we failed to find a self-consistent solution, don't bother
            %solving...
            for l=1:size(n,2)
                if doselfcon && flag <-1
                    d1 = Inf; d2 = Inf; d3 = Inf;
                    warning('Could not find a self-consistent solution for case %d',ind);
                else
                    % Intersection of O-lattice planes
                    % and the interface
                    %nT1 was computed above
                    nT2=cross(T2,n(:,l));
                    nT3=cross(T3,n(:,l));

                    %n1 was computed above
                    n2=norm(nT2);
                    n3=norm(nT3);

                    %compute the defect spacings 
                    d1 = 1/n1(l);  d2 = 1/n2;  d3 = 1/n3;
                end

                %check if any are larger than the maximum spacing, dmax
                set = 0;
                if d1 > dmax
                    d1 = Inf;
                    ksi1=[0 0 0]';
                else
                    set = set+1;
                    ksi1=nT1(:,l)/n1(l);
                end

                if d2 > dmax
                    d2 = Inf;
                    ksi2=[0 0 0]';
                else
                    set = set+1;
                    ksi2=nT2/n2;  
                end

                if d3 > dmax
                    d3 = Inf;
                    ksi3=[0 0 0]';
                else
                    set = set+1;
                    ksi3=nT3/n3;   
                end

                if l==1
                    index = index+1;
                end

                ksi1(abs(ksi1)<eps1) = 0.0;
                ksi2(abs(ksi2)<eps1) = 0.0;
                ksi3(abs(ksi3)<eps1) = 0.0; 

                no=no+1;

                % Calculating Solution sorting parameters P|Q|R
                bvec = [norm(b1) norm(b2) norm(b3)];
                dvec = [d1 d2 d3];
                P = sum(bvec.^2./dvec.^2);
                Q = P + 2*bvec(1)*bvec(2)/(dvec(1)*dvec(2)) +...
                    2*bvec(2)*bvec(3)/(dvec(2)*dvec(3)) +...
                    2*bvec(1)*bvec(3)/(dvec(1)*dvec(3)); 
                R = sum(sqrt(bvec.^2./dvec.^2)) +... 
                    2*sqrt(bvec(1)*bvec(2)/(dvec(1)*dvec(2))) +...
                    2*sqrt(bvec(2)*bvec(3)/(dvec(2)*dvec(3))) +...
                    2*sqrt(bvec(1)*bvec(3)/(dvec(1)*dvec(3)));

                %sort for output in order of smallest spacing, also accounting
                %for Burgers vectors and line directions
                ksis = [ksi1 ksi2 ksi3];
                bs = [b1 b2 b3];
                ds = [round(d1,6) round(d2,6) round(d3,6)]; %round to enable detection of identical solutions
                [~,inds] = sortrows([ds; bs; ksis]');

                %if there's a disconnection, always put it first 
                if abs(h)>0
                    inds(inds==1) = [];
                    inds = [1; inds];
                end

                %no habit plane angle if self consistency off 
                if doselfcon == 0
                    angs(1) = NaN;
                    angs(2) = NaN;
                end

                % Saving output data in a matrix
                Mat(no,:) = [index set P Q R ...
                                ksis(:,inds(1))' bs(:,inds(1))' ds(inds(1))' ...  
                                ksis(:,inds(2))' bs(:,inds(2))' ds(inds(2))' ...
                                ksis(:,inds(3))' bs(:,inds(3))' ds(inds(3))' ...
                                h atan2d(h,d1) n(:,l)' angs...
                                reshape(UA,[1 9]) reshape(UB,[1 9])];
            end
            if mod(index,5000)==0
                fprintf('Completed %d...\n',index)
            end
        end
    end
end
%get rid of zeros at the end...
Mat(no+1:end,:) = [];

% Finding independent solutions by comparing all line defects and step
% heights
if norepeats
    % if the spacing is infinite, zero out the Burgers vector and step height before comparing
    tmp = [Mat(:,6:27)];
    tmp(isinf(tmp(:,7)),[4:6 22]) = 0;
    tmp(isinf(tmp(:,14)),11:13) = 0;
    tmp(isinf(tmp(:,21)),18:20) = 0;
    % find the unique rows within tolerance of eps1
    [~,inds,~] = uniquetol(tmp,eps1,'ByRows',true);
    MatI = Mat(inds,:);
    fprintf('Number of independent solutions: %d\n',size(MatI,1))
else 
    MatI = Mat;
end

% Sort based on the chosen sorting parameter
if srt > 0 
    MatI=sortrows(MatI,srt+2); % if 0, don't sort it
end

% Write output
fprintf('Saving results to %s\n',fname)
fid2=fopen(fname,'wt');
fprintf(fid2,['units = ' unt '\n']);
fprintf(fid2,['nFA = ' '\n']);
fprintf(fid2,'%e ',FAn);
fprintf(fid2,'\n');
fprintf(fid2,['nFB = ' '\n']);
fprintf(fid2,'%e ',FBn);
fprintf(fid2,'\n');
fprintf(fid2,'%6s\t%2s\t',["No", "Set"]);
fprintf(fid2,'%10s\t',["P", "Q", "R"]);
fprintf(fid2,'%25s\t%30s\t%22s\t',["ksi1", "b1", "d1"]);
fprintf(fid2,'%25s\t%30s\t%22s\t',["ksi2", "b2", "d2"]);
fprintf(fid2,'%25s\t%30s\t%22s\t',["ksi3", "b3", "d3"]);
fprintf(fid2,'%10s\t%10s\t',["h", "phi"]);
fprintf(fid2,'%22s\t',"n");
fprintf(fid2,'%22s\t%10s\t',["ang1", "ang2"]);
fprintf(fid2,'%60s\t%120s\t',["UA", "UB"]);
fprintf(fid2,'\n');
for i = 1:size(MatI,1)
    fprintf(fid2,'%6d\t%6d\t',MatI(i,1:2));
    fprintf(fid2,'%10.4e\t',MatI(i,3:32));
    fprintf(fid2,'%10.4e\t%15.4e\t',MatI(i,33:34));
    fprintf(fid2,'%10.4e\t',MatI(i,35:end));    fprintf(fid2,'\n');    
end
fclose(fid2);

end