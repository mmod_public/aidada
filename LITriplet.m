%enumerate all triplets of linearly independent Burgers vectors from a
%given list
function [bt,ht] = LITriplet(b,h)
%choose the sign so that the first nonzero entry is positive
for i=1:size(b,1)
    b(i,1:3) = sign(b(i,find(abs(b(i,1:3))>1e-6,1)))*b(i,1:3);
end
%remove repeated line defects
tmp = uniquetol([b h],1e-6,'ByRows',true);
b = tmp(:,1:3);
h = tmp(:,4);

%sort in order of h magnitude so that disconnections are always listed
%first
[~,I] = sort(abs(h),'descend');
h = h(I,:);
b = b(I,:);

l = 1;
bt = [];
ht = [];
for i=1:size(b,1)
    for j=i+1:size(b,1)
        for k=j+1:size(b,1)
            %ensure there is at most just one disconnection in the triplet,
            %use condition number to assess linear independence
            if sum([abs(h(i,1))>0 abs(h(j,1))>0 abs(h(k,1))>0])<=1 && cond([b(i,:); b(j,:); b(k,:)])<1e10
                bt(l,:) = [b(i,:) b(j,:) b(k,:)];
                ht(l,:) = h(i,:);
                l = l+1;
            end
        end
    end
end
end