function [MM]=SymElm(struct)
% ------------------------------------------------------
% SUBROUTINE SYMMETRY ELEMENTS -------------------------
% This MATLAB subroutine creates all symmetry operation
% matrices of cubic crystals.

if struct~="FCC" && struct~="BCC"
    error('Invalid crystal structure type, must be cubic')
end

dirs = [1  0  0;
       -1  0  0;
        0  1  0;
        0 -1  0;
        0  0  1;
        0  0 -1];
ndirs = size(dirs,1);
count = 1;
for i=1:ndirs
    for j=1:ndirs
        xdir = dirs(i,:);
        ydir = dirs(j,:);
        %skip if they are point in the same direction
        if abs(abs(dot(xdir,ydir))-1)>1e-8
            zdir = cross(xdir,ydir);
            zdir = zdir/norm(zdir);
            MM{count} = [xdir' ydir' zdir'];
            count  = count+1;
        end
    end
end
end