%find the habit plane resulting from a disconnection array
function [np,flag,angs] = find_habit_plane(h,n,T1)
try_all = 0;
nang = 10;
eps = 1e-4;
%if no step, just pass through
if h==0
    np = n;
    flag = 1;
    angs = [0 0];
else
    %nonlinear rootfinder to determine habit plane normal np
    options = optimoptions('fsolve','algorithm','levenberg-marquardt','StepTolerance',1e-10,'FunctionTolerance',1e-10,'Display','off');
    
    %make sure the solution isn't a NaN at the first iteration
    tmp = FB_HP_Eqns_Angs(h,n,T1,[pi/2 pi/2]);
    if any(isnan(tmp))
        np = [0; 0; 0];
        flag = -2;
    else
        %loop over a range of initial guesses
        %if try_all==1, try them all and store all unique solutions (runs
        %slower)
        %if try_all==0, exit once one solution is attained (runs faster)
        solns = [];
        for i=1:nang
            for j=1:nang
                [angs,~,flag] = fsolve(@(angs) FB_HP_Eqns_Angs(h,n,T1,angs),[i/(nang+1) j/(nang+1)].*[pi pi],options);
                if flag>=-1
                    np = angs_to_vec(angs);
                    np = sign(np(2))*np;
                    np(abs(np)<1e-4) = 0;   %ismembertol struggles with very small numbers, set anything small to zero
                    if isempty(solns) || ismembertol(np',solns',eps,'ByRows',true)==0
                        solns = [solns np];
                        if try_all==0
                            break;
                        end
                    end
                end
            end
            if ~isempty(solns) && try_all==0
                break;
            end
        end
        %if no solution is found, return zero vector
        if isempty(solns)
            np = [0; 0; 0];
            flag = -2;
        else
            flag = 1;
        end
    end
end
end

function res = FB_HP_Eqns_Angs(h,n,T1,angs)
np = angs_to_vec(angs);
%make sure np points in the +y direction (otherwise sense vectors are
%wrong)
np = sign(np(2))*np;

%from O-lattice solution
nT1 = cross(T1,np);
n1 = norm(nT1);
d1 = 1/n1;
ksi1 = nT1/n1;

%habit plane geometry
phi = atan2(h,d1);
npnew = axis_angle_rotate(n,ksi1,phi);

%habit plane angles
angsnew(1) = acos(npnew(3));
angsnew(2) = sign(npnew(2))*acos(npnew(1)/sqrt(npnew(1)^2+npnew(2)^2));

%residual
res=angs-angsnew;
end

function n = angs_to_vec(angs)
theta = angs(1);
phi = angs(2);
n = [sin(theta)*cos(phi); sin(theta)*sin(phi); cos(theta)];
end