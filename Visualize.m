function Visualize(outputfile,sol,rng)
fid = fopen(outputfile);
tmp = fgetl(fid);
unt = tmp(9:end);
fclose(fid);

%load the results for this outputfile
RA = reshape(dlmread(outputfile,' ',[2 0 2 8]),[3 3]);
RB = reshape(dlmread(outputfile,' ',[4 0 4 8]),[3 3]);

if nargin<2
    sol = 1;
end
tmp = dlmread(outputfile,'\t',6,0);
ksi1 = tmp(sol,6:8);
d1 = tmp(sol,12);
ksi2 = tmp(sol,13:15);
d2 = tmp(sol,19);
ksi3 = tmp(sol,20:22);
d3 = tmp(sol,26);
n = tmp(sol,29:31);

Ds = [d1 d2 d3];
Dmin = min(Ds);
if nargin<3
    rng = 5*Dmin;
end

KSIs = [ksi1 ksi2 ksi3];

if (isinf(Dmin))
    disp('----------------------------------------------------------------------------- ');
    disp('The solution with the lowest sorting parameter value contains no dislocations ');
    disp('----------------------------------------------------------------------------- ');
else
    
    col1 =[0 0.4470 0.7410];           % Line Color 1
    col2 =[0.8500 0.3250 0.0980];           % Line Color 2
    col3 =[0.4660 0.6740 0.1880];           % Line Color 3
    col4 =[0.1255 , 0.6980 , 0.667];   % Color 4
    col5 =[0 , 1 , 0.6039];            % Color 5
    cols = [col1; col2; col3];

    % ---------------------------------------------- 3D PLOT

    figure(10)
    set(gcf,'Position', [550, 0, 750, 600]); % l/b/w/h

    plot3([-rng -rng],[0 0],[-0.7*rng -rng],'b', ...
          [-rng -0.7*rng],[0 0],[-rng -rng],'b', ...
          [-rng -rng],[0 0.3*rng],[-rng -rng],'b', ...
          'linewidth',3 );
    %plotA = [-rng -rng],[0 0],[-0.7*rng -rng]
    %plotB = [-rng -0.7*rng],[0 0],[-rng -rng]
    %plotC = [-rng -rng],[0 0.3*rng],[-rng -rng]
    hold on

    % Lables of coordinate axes --------------- Lables X Y Z
    text(-0.75*rng, 0.02 *rng, -1.07*rng, ...
        'X' , 'FontSize',18 , 'Color', 'b')
    text(-1.1*rng, 0.27 *rng, -1.07*rng, ...
        'Y' , 'FontSize',18 , 'Color', 'b') 
    text(-1.1 *rng,0.01 *rng, -0.75*rng, ...
        'Z' , 'FontSize',18 , 'Color', 'b')

    % Terrace plane ---------------------------- Interface
    fill3([rng -rng -rng rng], ...
          [0 0 0 0], ...
          [rng rng -rng -rng], ...
          [0 0 0],'linewidth',2,'facealpha',0.15)

    % Habit plane
    if abs(n(1))>1e-6 || abs(n(3))>1e-6
        hpcorners = [-n(1)*rng-n(3)*rng n(1)*rng-n(3)*rng n(1)*rng+n(3)*rng -n(1)*rng+n(3)*rng]/n(2);
        fill3([rng -rng -rng rng], ...
              hpcorners, ...
              [rng rng -rng -rng], ...
              [1 1 1],'linewidth',2,'facealpha',0)
    else 
        hpcorners = [-rng rng];
    end
%     alpha(0.5)
    
    X = -10*rng:10*rng:10*rng;
    for i=1:3
        if Ds(i)<Inf
            ksii = KSIs(3*(i-1)+(1:3));
            Dvec = cross(n,ksii);
            Dvec = Ds(i)*Dvec/norm(Dvec);
            for j=-10*rng/Ds(i):1:10*rng/Ds(i)
                plot3(X*ksii(1)+j*Dvec(1),X*ksii(2)+j*Dvec(2),X*ksii(3)+j*Dvec(3),'Color',cols(i,:), ...
                      'LineWidth',3);
            end
        end
    end
      
    % Scale bar ---------------------------------- Scale bar
    if (rng<=10)
        % ---- Scale
        text(0,-0.7*rng,-1.07*rng,['1 ' unt] , ...
        'color',[0,0,0] , ...
        'FontSize', 18 , ...
        'HorizontalAlignment','center' )
        % ---- Bar
        plot3([-0.5;  -0.5], ...
              [-0.47*rng ; -0.47*rng], ...
              [-0.988*rng;  -0.988*rng], '-k', ...
              [-0.5;   0.5], ...
              [-0.47*rng ; -0.47*rng], ...
              [-0.988*rng;  -0.988*rng], '-k', ...
              'LineWidth', 6)
    elseif ( (rng>10) && (rng<=100) )
        % ---- Scale
        text(0,-0.7*rng,-1.07*rng,['10 ' unt] , ...
        'color',[0,0,0] , ...
        'BackgroundColor',[0.9,0.9,0.9] , ...
        'FontSize', 18 , ...
        'HorizontalAlignment','center' )
        % ---- Bar
        plot3([-5;  -5], ...
              [-0.47*rng ; -0.47*rng], ...
              [-0.988*rng;  -0.988*rng], '-k', ...
              [-5;   5], ...
              [-0.47*rng ; -0.47*rng], ...
              [-0.988*rng;  -0.988*rng], '-k', ...
              'LineWidth', 6)
    elseif ( (rng>100) && (rng<=1000) )
        % ---- Scale
        text(0,-0.7*rng,-1.07*rng,['100 ' unt] , ...
        'color',[0,0,0] , ...
        'BackgroundColor',[0.9,0.9,0.9] , ...
        'FontSize', 18 , ...
        'HorizontalAlignment','center' )
        % ---- Bar
        plot3([-50;  -50], ...
              [-0.47*rng ; -0.47*rng], ...
              [-0.988*rng;  -0.988*rng], '-k', ...
              [-50;   50], ...
              [-0.47*rng ; -0.47*rng], ...
              [-0.988*rng;  -0.988*rng], '-k', ...
              'LineWidth', 6)
    elseif (rng>1000) 
        % ---- Scale
        text(0,-0.7*rng,-1.07*rng,['1000 ' unt] , ...
        'color',[0,0,0] , ...
        'BackgroundColor',[0.9,0.9,0.9] , ...
        'FontSize', 18 , ...
        'HorizontalAlignment','center' )
        % ---- Bar
        plot3([-500;  -500], ...
              [-0.47*rng ; -0.47*rng], ...
              [-0.988*rng;  -0.988*rng], '-k', ...
              [-500;   500], ...
              [-0.47*rng ; -0.47*rng], ...
              [-0.988*rng;  -0.988*rng], '-k', ...
              'LineWidth', 6)
    end

    axis off

    % Plot view --------------------------------------- View
    az = -45 ;
    el =  15 ;
    view(az,el);

    rotate3d on

    % cameratoolbar('SetCoordSys','y')

    % Unit cube at origin
    x=([0 1 1 0 0 0;1 1 0 0 1 1; ...
        1 1 0 0 1 1;0 1 1 0 0 0]-0.5);
    y=([0 0 1 1 0 0;0 1 1 0 0 0; ...
        0 1 1 0 1 1;0 0 1 1 1 1]-0.5);
    z=([0 0 0 0 0 1;0 0 0 0 0 1; ...
        1 1 1 1 0 1;1 1 1 1 0 1]-0.5);

    xxa=zeros(1,1);
    yya=zeros(1,1);
    zza=zeros(1,1);
    xxb=zeros(1,1);
    yyb=zeros(1,1);
    zzb=zeros(1,1);

        % Crystal A ---------------------------------- Crystal A
        for i=1:4
            for j=1:6
               veci = [ x(i,j) ; y(i,j) ; z(i,j) ] ; 
               veco=RA*veci;
               xxa(i,j)=veco(1);
               yya(i,j)=veco(2);
               zza(i,j)=veco(3);
            end
        end

        origin=[-0.8*rng 0.55*rng -0.8*rng];
        sizei=0.2*rng;
        xa=xxa*sizei+origin(1);
        ya=yya*sizei+origin(2);
        za=zza*sizei+origin(3);

        for i=1:6 
            patch(xa(:,i),ya(:,i),za(:,i), col4 , ...
                'facealpha', 1.0 , ...
                'edgecolor','w', ...
                'LineWidth' , 1);
        end 

        text(-0.8*rng, 0.55*rng, -0.8*rng,'A' , ...
            'color',[0,0,0] , ...
            'FontSize', 18 , ...
            'HorizontalAlignment','center' )

        % Crystal B ---------------------------------- Crystal B
        for i=1:4
            for j=1:6
               veci = [ x(i,j) ; y(i,j) ; z(i,j) ] ; 
               veco=RB*veci;
               xxb(i,j)=veco(1);
               yyb(i,j)=veco(2);
               zzb(i,j)=veco(3);
            end
        end

        origin=[-0.8*rng -0.55*rng -0.8*rng];
        sizei=0.2*rng;
        xb=xxb*sizei+origin(1);
        yb=yyb*sizei+origin(2);
        zb=zzb*sizei+origin(3);

        for i=1:6 
            patch(xb(:,i),yb(:,i),zb(:,i), col5 , ...
                'facealpha', 1.0 , ...
                'edgecolor','w', ...
                'LineWidth' , 1 );
        end

        text(-0.8*rng, -0.55*rng, -0.8*rng,'B' , ...
            'color',[0,0,0] , ...
            'FontSize', 18 , ...
            'HorizontalAlignment','center' )

    %end % if (RFF~=1)

%     daspect([max(daspect)*[1 1] 1]);
%     zlim([-1.0*rng 1.0*rng]);
    rotate3d on
    
    xlim([-rng rng])
%     ylim([min(hpcorners) max(hpcorners)])
    ylim([-rng rng])
    zlim([-rng rng])
    daspect([1 1 1])
    
    hold off

end % if (st==0)

end