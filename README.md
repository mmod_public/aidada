# AIDADA

AIDADA (Arrangement of Interfacial Dislocation and Disconnection Arrays) provides all solutions to the Frank-Bilby equation for a specified interface and list of line defects. The code was originated by Sangghaleh and Demkowicz (2018), and then subsequently expanded and re-released by Gordon and Sills (2024). Users must define two states for the interface:
1. The natural state wherein the crystals are oriented as desired and strain-free 
2. A coherent reference state wherein the crystals are coherent and may contain misfit strains.

Users must also define a list of line defects (dislocations and disconnections) which could be present in the interface. AIDADA obtains solutions for all possible combinations of line defects and all admissible symmetry operations.

To run AIDADA, simply call the function:<br /> 
```AIDADA(Astruct,Bstruct,aA,aB,xAn,xBn,yAn,yBn,xAc,xBc,yAc,yBc,unt,srt,BurgersList,CrysList,SA,SB,Fr,dmax,doselfcon,norepeats,fname)```<br /> 
with all necessary inputs specified. For convenience, it is suggested that an input script is used to define all inputs. Input scripts used to generate results in Gordon and Sills (2024) are available in the ```inputs``` folder. Inputs are as follows:
- ```Astruct``` = Crystal structure type for crystal A
- ```Bstruct``` = Crystal structure type for crystal B
- ```aA``` = Lattice constant of Crystal A
- ```aB``` = Lattice constant of Crystal B
- ```xAn``` = x direction of Crystal A in the natural state
- ```yAn``` = y direction of Crystal A in the natural state
- ```xBn``` = x direction of Crystal B in the natural state
- ```yBn``` = y direction of Crystal B in the natural state
- ```xAc``` = x direction of Crystal A in the coherent reference state (set to [] if same as ```xAn```)
- ```yAc``` = y direction of Crystal A in the coherent reference state (set to [] if same as ```yAn```)
- ```xBc``` = x direction of Crystal B in the coherent reference state (set to [] if same as ```xBn```)
- ```yBc``` = y direction of Crystal B in the coherent reference state (set to [] if same as ```yBn```)
- ```unt``` = unit used for the lattice parameter (string)
- ```srt``` = Solution sorting parameter (P=1 | Q=2 | R=3) (integer)
- ```Burgers_List``` = Cell array of strings referring to text files containing Burgers vectors to use in the calculation
- ```CrysList``` = String array specifying the coordinate system used to  define each respective Burgers_List entry; A = Crystal A, B = Crystal B, INT = interface
- ```SA``` = Stretch tensor for the natural state of Crystal A
- ```SB``` = Stretch tensor for the natural state of Crystal B
- ```Fr``` = Deformation gradient to deviate from the default coherent reference state, if desired
- ```dmax``` = maximum defect spacing allowed, if d>dmax it is set to inf
- ```doselfcon``` = Binary flag (0 or 1) indicating whether to enforce self-consistency in the habit plane orientation
- ```norepeats``` = Binary flag (0 or 1) indicating whether to remove repeated solutions from the solution list
- ```fname``` = String indicating the file name (plus extension) to write results to

## Defining line defects

Line defects used to construct solutions are specified as text files (in the ```burgers``` folder) using the following format:
- Line 1: Description of file contents (no formatting constraints)
- Line 2: Text defining the reference frame for the Burgers vectors, the following options are currently available
    - BCC = BCC unit cell reference frame
    - FCC = FCC unit cell reference frame
    - INT = interface reference frame
- Line 3: Comment line (not used)
- Line 4 and up: Define Burgers vectors and step heights for each line defect, one per line with each column entry separated by a comma
    - Columns 1-3 give the Burgers vector components in the specified reference frame in x, y, z order
    - Column 4 specifies the step height. A height of 0 denotes a dislocation, non-zero step height denotes a disconnection. Disconnections should be listed with both positive and negative step heights to ensure all possible solutions are explored (see Gordon and Sills (2024) for more info). .

All quantities must be normalized by the relevant lattice constant:
- FCC lattice constant for FCC 
- BCC lattice constant for BCC 
- Average of the crystal lattice constants for INT

Users may specify new line defects beyond those given in this repository by adding their own text files. If a user wishes to specify line defects for a different reference frame from those given above, please contact the developers.

In the input file, a user must specify for each line defect file what crystal is used to define the coherent reference state. Care must be taken to ensure that the specified Burgers vectors are consistent with the specified coherent reference state (e.g., only use FCC defects for FCC crystals). 


 ## Defining crystal symmetry operators

The function ```SymElm``` returns as a cell array all crystal symmetry operators (e.g., rotation matrices) for a given crystal structure. Currently only cubic crystals are supported. If a different crystal structure is desired, the user must extend the ```SymElm``` function to return all relevant symmetry operators. Please contact the developers for support. 

## Structure of the results file

Results are stored in a tabular text file (with path and name ```fname```) with the following format:
- Line 1: units used in the file
- Line 2-3: Deformation gradient defining the natural reference state of crystal A 
- Line 4-5: Deformation gradient defining the natural reference state of crystal B 
- Line 6: Column labels
- Line 7 and up: Obtained solutions, one per line
    - Column 1: Solution number
    - Column 2: Line defect set number
    - Column 3-5: P, Q, and R values for the solution
    - Column 6-12: line direction vector, Burgers vector, defect spacing for defect array 1  
    - Column 13-19: line direction vector, Burgers vector, defect spacing for defect array 2
    - Column 20-26: line direction vector, Burgers vector, defect spacing for defect array 3 
    - Column 27: Step height for disconnection (defect array 1)
    - Column 28: Rotation angle of the habit plane about the disconnection line
    - Column 29-31: Habit plane normal vector in Cartesian coordinates
    - Column 32-33: Habit plane normal vector angles for spherical
    - Column 34-42: Symmetry operator for crystal A (column-wise) 
    - Column 43-51: Symmetry operator for crystal B (column-wise)    

All vectors are given in the coherent reference state.

## Visualization of results

Solutions can be visualized using the ```Visualize``` function as follows:<br />
```Visualize(outputfile,sol,rng)```<br />
(arguments 2 and 3 are optional) with function inputs being: 
- ```outputfile``` = path and filename for the chosen results file
- ```sol``` = solution number to be visualized, if omitted solution 1 is used
- ```rng``` = plot axis range, if omitted a default value is used

## References

A. Sangghaleh and M. J. Demkowicz, Comp. Mater. Sci., Vol. 145, p. 35-47, (2018).<br />
D. B. Gordon and R. B. Sills, J. Mech. Phys. Sol., Vol. 193, 105845, (2024).

## License
AIDADA is open-source software, see the LICENSE file for more details.

## Authors and acknowledgment
AIDADA was developed by David Gordon and Prof. Ryan Sills of the Department of Materials Science and Engineering at Rutgers University. Its development was supported by the U.S. Department of Energy, Office of Science, Basic Energy Sciences, under Award # DE-SC0022154. The AIDADA algorithm is documented in this [journal article](https://doi.org/10.1016/j.jmps.2024.105845).
